import { Container, Heading, Text } from '@/styles/Hero.styles';

export default function Showcase() {
  return (
    <Container>
      <Heading>Welcome To The Classical Events</Heading>
      <Text>Find the Classical Events that you love</Text>
    </Container>
  );
}
