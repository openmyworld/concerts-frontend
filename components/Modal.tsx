import React, { useState, useEffect } from 'react';
import ReactDOM from 'react-dom';
import { Overlay, ModalTag, ModalBody, Header } from '@/styles/Modal.styles';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faTimes } from '@fortawesome/free-solid-svg-icons';

type Props = {
  show: boolean;
  children: React.ReactNode;
  onClose: () => void;
  title?: string;
};

export default function Modal({ show, onClose, children, title }: Props) {
  const [isBrowser, setIsBrowser] = useState(false);

  const handleClose = (e: React.MouseEvent<HTMLAnchorElement>) => {
    e.preventDefault();
    onClose();
  };

  useEffect(() => setIsBrowser(true), []);

  const modalContent = show ? (
    <Overlay>
      <ModalTag>
        <Header>
          <a href='#' onClick={handleClose}>
            <FontAwesomeIcon icon={faTimes} />
          </a>
        </Header>
        {title && <p>{title}</p>}
        <ModalBody>{children}</ModalBody>
      </ModalTag>
    </Overlay>
  ) : null;

  if (isBrowser) {
    // Make equal type checking
    const modalRoot: HTMLElement | null = document.getElementById('modal-root');

    return ReactDOM.createPortal(modalContent, modalRoot as HTMLElement);
  } else {
    return null;
  }
}
