import React, { useState } from 'react';
import { useRouter } from 'next/router';
import { Input } from '@/styles/Search.styles';

export default function Search() {
  const [term, setTerm] = useState('');
  const router = useRouter();

  const handleSubmit = (e: React.FormEvent<HTMLFormElement>) => {
    e.preventDefault();
    router.push(`/concerts/search/?term=${term}`);
    setTerm('');
  };

  return (
    <form onSubmit={handleSubmit}>
      <Input
        type='text'
        value={term}
        onChange={(e) => setTerm(e.target.value)}
        placeholder='Search concert'
      />
    </form>
  );
}
