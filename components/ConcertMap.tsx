import { useState, useEffect } from 'react';
import Image from 'next/image';

import ReactMapGl, {
  Marker,
  FullscreenControl,
  ScaleControl,
  NavigationControl,
} from 'react-map-gl';

import 'mapbox-gl/dist/mapbox-gl.css';

type Props = {
  address: string;
  id: number;
};

const api: string | undefined = process.env.NEXT_PUBLIC_MAPBOX_API_TOKEN;

const fullscreenControlStyle = {
  top: 36,
  left: 0,
  padding: '10px',
};

const scaleControlStyle = {
  bottom: 36,
  left: 0,
  padding: '10px',
};

const navStyle = {
  top: 72,
  left: 0,
  padding: '10px',
};

export default function ConcertMap({ address, id }: Props) {
  // const [lng, setLng] = useState<number | null>(null);
  // const [lat, setLat] = useState<number | null>(null);
  const [lng, setLng] = useState(0);
  const [lat, setLat] = useState(0);
  const [loading, setLoading] = useState(true);

  const [viewport, setViewport] = useState({
    latitude: 40.800904916601986,
    longitude: -73.94973158733607,
    zoom: 12,
  });

  useEffect(() => {
    const getGeoCoding = async (address: string) => {
      try {
        const endpoint = `https://api.mapbox.com/geocoding/v5/mapbox.places/${encodeURIComponent(
          address
        )}.json?access_token=${api}`;

        const response = await fetch(endpoint);

        const result = await response.json();

        if (response.ok) {
          const lngResponse = result.features[0].geometry.coordinates[0];
          const latResponse = result.features[0].geometry.coordinates[1];

          setLat(latResponse);
          setLng(lngResponse);

          setViewport({
            ...viewport,
            latitude: latResponse,
            longitude: lngResponse,
          });

          setLoading(false);
        }
      } catch (e) {
        console.error(e);
      }
    };

    getGeoCoding(address);
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);

  if (loading) {
    return <p>Loading</p>;
  } else {
    return (
      <ReactMapGl
        {...viewport}
        width='100%'
        height='500px'
        mapboxApiAccessToken={api}
        mapStyle='mapbox://styles/mapbox/streets-v11'
        onViewStateChange={setViewport}
      >
        <Marker key={id} latitude={lat} longitude={lng}>
          <Image src='/images/pin.svg' alt='Marker' width={20} height={20} />
        </Marker>
        <FullscreenControl style={fullscreenControlStyle} />
        <ScaleControl style={scaleControlStyle} />
        <NavigationControl style={navStyle} />
      </ReactMapGl>
    );
  }
}
