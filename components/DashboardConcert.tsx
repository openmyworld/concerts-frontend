import Link from 'next/link';

import {
  Container,
  Heading,
  Anchor,
  DeleteLink,
  HandleBox,
} from '@/styles/DashboardConcert.styles';

import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faEdit, faTrash } from '@fortawesome/free-solid-svg-icons';

type Props = {
  concert: {
    slug: string;
    id: number;
    name: string;
  };

  handleDelete: (id: number) => void;
};

export default function DashboardConcert({ concert, handleDelete }: Props) {
  return (
    <Container>
      <Heading>
        <Link href={`/concerts/${concert.slug}`} passHref>
          <Anchor>{concert.name}</Anchor>
        </Link>
      </Heading>

      <HandleBox>
        <Link href={`/concerts/edit/${concert.id}`} passHref>
          <Anchor>
            <FontAwesomeIcon icon={faEdit} /> Edit
          </Anchor>
        </Link>
        <DeleteLink href='#' onClick={() => handleDelete(concert.id)}>
          <FontAwesomeIcon icon={faTrash} /> Delete
        </DeleteLink>
      </HandleBox>
    </Container>
  );
}
