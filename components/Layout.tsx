import React from 'react';
import Head from 'next/head';
import Header from './Header';
import Footer from './Footer';
import Hero from './Hero';
import { Container } from '@/styles/Layout.styles';
import { useRouter } from 'next/router';
import { GlobalStyle } from '@/styles/Globals.styles';

type LayoutProps = {
  title?: string;
  description?: string;
  children: React.ReactNode;
};

export default function Layout({
  title = 'Concert listings on Classical Events',
  description = 'Find concerts in your area with Classical Events.',
  children,
}: LayoutProps) {
  const router = useRouter();

  return (
    <>
      <Head>
        <title>{title}</title>
        <meta name='description' content={description} />
      </Head>

      <Header />

      <GlobalStyle />

      {router.pathname === '/' && <Hero />}

      <Container>{children}</Container>

      <Footer />
    </>
  );
}
