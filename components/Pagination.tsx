import Link from 'next/link';
import { PER_PAGE } from '@/config/index';
import { Container } from '@/styles/Pagination.styles';
import { ButtonSecondary } from '@/styles/Button.styles';

type Props = {
  page: number;
  total: number;
};

export default function Pagination({ page, total }: Props) {
  const lastPage = Math.ceil(total / PER_PAGE);

  return (
    <Container>
      {page > 1 && (
        <Link href={`/concerts?page=${page - 1}`} passHref>
          <ButtonSecondary>Prev</ButtonSecondary>
        </Link>
      )}

      {page < lastPage && (
        <Link href={`/concerts?page=${page + 1}`} passHref>
          <ButtonSecondary>Next</ButtonSecondary>
        </Link>
      )}
    </Container>
  );
}
