import { Container } from '@/styles/Footer.styles';

export default function Footer() {
  const currentYear = new Date().getFullYear();

  return (
    <Container>
      <p>Copyright &copy; {currentYear} Classical Events</p>
    </Container>
  );
}
