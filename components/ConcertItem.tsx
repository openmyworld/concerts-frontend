import Link from 'next/link';
import Image from 'next/image';

import {
  Info,
  ImageContainer,
  Heading,
  LinkContainer,
  Paragraph,
} from '@/styles/ConcertItem.styles';

import { formatDate, formatTimeOneParam } from '@/helpers/index';

type concertItemProps = {
  concert: {
    name: string;
    published_at: string;
    address: string;
    image: {
      formats: {
        thumbnail: {
          url: string;
        };
      };
    };
    slug: string;
  };
};

export default function ConcertItem({ concert }: concertItemProps) {
  const date = formatDate(concert.published_at);
  const time = formatTimeOneParam(concert.published_at);

  return (
    <li>
      <Link href={`/concerts/${concert.slug}`} passHref>
        <LinkContainer>
          <ImageContainer>
            <Image
              src={
                concert.image
                  ? concert.image.formats.thumbnail.url
                  : '/images/no-image.jpg'
              }
              layout='fill'
              alt={concert.name}
            />
          </ImageContainer>
          <Info>
            <time dateTime={date}>
              {date} at {time}
            </time>
            <Heading>{concert.name}</Heading>
            <Paragraph>{concert.address}</Paragraph>
          </Info>
        </LinkContainer>
      </Link>
    </li>
  );
}
