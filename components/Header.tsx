import { useContext } from 'react';
import Link from 'next/link';

import { HeaderTag, Logo } from '@/styles/Header.styles';
import Search from './Search';
import AuthContext, { contextValue } from '@/context/AuthContext';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';

import {
  faPlus,
  faHome,
  faQuestion,
  faBox,
  faSignInAlt,
  faSignOutAlt,
} from '@fortawesome/free-solid-svg-icons';

import { Navbar, Nav, NavDropdown, Container } from 'react-bootstrap';
import 'bootstrap/dist/css/bootstrap.min.css';

export default function Header() {
  const { user, logout } = useContext(AuthContext) as contextValue;

  return (
    <HeaderTag>
      <Navbar bg='light' expand='lg' fixed='top'>
        <Container>
          <Link href='/' passHref>
            <Navbar.Brand>
              <Logo src='/piano.png' />
            </Navbar.Brand>
          </Link>

          <Navbar.Toggle aria-controls='basic-navbar-nav' />
          <Navbar.Collapse id='basic-navbar-nav'>
            <Nav className='ms-auto'>
              <Link href='/concerts' passHref>
                <Nav.Link>
                  <FontAwesomeIcon icon={faBox} /> Concerts
                </Nav.Link>
              </Link>
              <Link href='/about' passHref>
                <Nav.Link href='/about'>
                  <FontAwesomeIcon icon={faQuestion} /> About
                </Nav.Link>
              </Link>
              {user ? (
                // When login
                <NavDropdown title={user.username} id='basic-nav-dropdown'>
                  <Link href='/concerts/add' passHref>
                    <NavDropdown.Item>
                      <FontAwesomeIcon icon={faPlus} /> Add concerts
                    </NavDropdown.Item>
                  </Link>
                  <Link href='/account/dashboard' passHref>
                    <NavDropdown.Item>
                      <FontAwesomeIcon icon={faHome} /> Dashboard
                    </NavDropdown.Item>
                  </Link>
                  <NavDropdown.Item onClick={() => logout()}>
                    <FontAwesomeIcon icon={faSignOutAlt} /> Logout
                  </NavDropdown.Item>
                </NavDropdown>
              ) : (
                // When logout
                <Link href='/account/login' passHref>
                  <Nav.Link>
                    <FontAwesomeIcon icon={faSignInAlt} /> Login
                  </Nav.Link>
                </Link>
              )}
            </Nav>
          </Navbar.Collapse>
        </Container>
      </Navbar>

      <Container>
        <Search />
      </Container>
    </HeaderTag>
  );
}
