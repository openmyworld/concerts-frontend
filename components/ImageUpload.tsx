import React, { useState } from 'react';
import { API_URL } from '@/config/index';
import { InputSubmit, UploadFile } from '@/styles/Form.styles';
import { Heading1 } from '@/styles/Heading.styles';

type Props = {
  concertId: number | string | Blob;
  imageUploaded: () => void;
  token: string;
};

export default function ImageUpload({
  concertId,
  imageUploaded,
  token,
}: Props) {
  const [image, setImage] = useState<string | Blob | null>(null);

  const handleSubmit = async (e: React.FormEvent<HTMLFormElement>) => {
    e.preventDefault();

    const formData = new FormData();

    formData.append('files', image as string | Blob);
    formData.append('ref', 'concerts');
    formData.append('refId', concertId as string);
    formData.append('field', 'image');

    const res = await fetch(`${API_URL}/upload`, {
      method: 'POST',
      headers: {
        Authorization: `Bearer ${token}`,
      },
      body: formData,
    });

    if (res.ok) {
      imageUploaded();
    }
  };

  const handleFileChange = (e: React.ChangeEvent<HTMLInputElement>) => {
    if (e.target.files !== null) {
      setImage(e.target.files[0]);
    }
  };

  return (
    <div>
      <Heading1>Upload Image Concert</Heading1>
      <form onSubmit={handleSubmit}>
        <div>
          <UploadFile onChange={handleFileChange} />
        </div>
        <InputSubmit as='input' type='submit' value='Upload' />
      </form>
    </div>
  );
}
