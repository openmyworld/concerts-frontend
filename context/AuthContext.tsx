import {
  createContext,
  useState,
  useEffect,
  Dispatch,
  SetStateAction,
} from 'react';

import { useRouter } from 'next/router';
import { NEXT_URL } from '@/config/index';

type Props = {
  children: React.ReactNode;
};

type loginParam = {
  identifier?: string;
  password?: string;
};

type userProperty = {
  username: string;
  email: string;
};

export type contextValue = {
  user: userProperty | null;
  error: string | null;
  register: (user: object) => void;
  login: ({ identifier, password }: loginParam) => void;
  logout: () => void;
  loadingLogin: boolean;
  setLoadingLogin: Dispatch<SetStateAction<boolean>>;
  loadingRegister: boolean;
  setLoadingRegister: Dispatch<SetStateAction<boolean>>;
};

const AuthContext = createContext<contextValue | null>(null);

export const AuthProvider = ({ children }: Props) => {
  const [user, setUser] = useState<userProperty | null>(null);
  const [error, setError] = useState<string | null>(null);
  const [loadingLogin, setLoadingLogin] = useState(false);
  const [loadingRegister, setLoadingRegister] = useState(false);

  const router = useRouter();

  // Register user
  const register = async (user: object) => {
    const response = await fetch(`${NEXT_URL}/api/register`, {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json',
      },
      body: JSON.stringify(user),
    });

    const data = await response.json();

    if (response.ok) {
      setLoadingRegister(false);
      setUser(data.user);
      router.push('/account/dashboard');
    } else {
      setError(data.message);
      setError(null);
    }
  };

  // Login user
  const login = async ({ identifier, password }: loginParam) => {
    const response = await fetch(`${NEXT_URL}/api/login`, {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json',
      },
      body: JSON.stringify({
        identifier,
        password,
      }),
    });

    const data = await response.json();

    if (response.ok) {
      setLoadingLogin(false);
      setUser(data.user);
      router.push('/account/dashboard');
    } else {
      setError(data.message);
      setError(null);
    }
  };

  // Logout user
  const logout = async () => {
    const response = await fetch(`${NEXT_URL}/api/logout`, {
      method: 'POST',
    });

    if (response.ok) {
      setUser(null);
      router.push('/account/login');
    }
  };

  // Check if user is logged in
  const checkUserLoggedIn = async () => {
    const response = await fetch(`${NEXT_URL}/api/user`);
    const data = await response.json();

    if (response.ok) {
      setUser(data.user);
    } else {
      setUser(null);
    }
  };

  useEffect(() => {
    checkUserLoggedIn();
  }, []);

  return (
    <AuthContext.Provider
      value={{
        user,
        error,
        register,
        login,
        logout,
        loadingLogin,
        setLoadingLogin,
        loadingRegister,
        setLoadingRegister,
      }}
    >
      {children}
    </AuthContext.Provider>
  );
};

export default AuthContext;
