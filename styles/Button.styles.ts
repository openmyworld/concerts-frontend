import styled from 'styled-components';

export const Button = styled.a`
  display: inline-block;
  background-color: #3a86ff;
  color: #fff;
  padding: 10px 20px;
  cursor: pointer;
  border: 1px solid #3a86ff;
  border-radius: 5px;
  transition: all 0.5s ease;

  &:hover {
    background-color: #fff;
    color: #3a86ff;
    border: 1px solid #3a86ff;
  }
`;

export const ButtonSecondary = styled.a`
  font: inherit;
  font-size: 16px;
  background-color: #000;
  color: #fff;
  border: 0;
  border-radius: 5px;
  padding: 5px 15px;
  margin: 0 20px;
  cursor: pointer;

  &:hover {
    opacity: 0.8;
  }

  @media (max-width: 768px) {
    font-size: 12px;
  }
`;
