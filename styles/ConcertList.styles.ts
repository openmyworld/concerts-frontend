import styled from 'styled-components';

export const Ul = styled.ul`
  list-style-type: none;
  margin: 0;
  padding-left: 0;
`;

export const ViewConcertContainer = styled.div`
  display: flex;
  justify-content: center;
`;
