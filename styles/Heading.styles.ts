import styled from 'styled-components';

export const Heading1 = styled.h1`
  @media (max-width: 768px) {
    font-size: 25px;
  }
`;

export const Heading2 = styled.h2`
  @media (max-width: 768px) {
    font-size: 20px;
  }
`;
