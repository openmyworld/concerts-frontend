import { createGlobalStyle } from 'styled-components';
import { normalize } from '@/styles/Reset.styles';

export const GlobalStyle = createGlobalStyle`
  ${normalize}
   
  html,
  body {
    font-family: 'Lato', sans-serif;
  }

  body {
    padding-top: 47px;
  }

  a {
    text-decoration: none;
  }
`;
