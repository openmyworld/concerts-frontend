import styled from 'styled-components';

export const HeaderTag = styled.header`
  /* display: flex;
  justify-content: space-between;
  align-items: center; */
  color: #333;
  padding: 20px;
  box-shadow: 0 2px 5px rgba(0, 0, 0, 0.1);

  /* @media (max-width: 768px) {
    flex-direction: column;
    height: auto;
    align-items: start;
  } */
`;

export const Ul = styled.ul`
  display: flex;
  align-items: center;
  justify-content: center;
  list-style: none;
  padding-left: 0;
  margin: 0;

  @media (max-width: 768px) {
    margin: 20px 0;
    flex-direction: column;
    align-items: start;
    width: 100%;
    box-sizing: border-box;
  }
`;

// export const LogoContainer = styled.figure`
//   margin: 0;

//   @media (max-width: 768px) {
//     width: 100%;
//     margin: 10px 0;
//   }
// `;

export const Logo = styled.img`
  display: block;
  width: 80px;
`;

export const Li = styled.li`
  /* .rc-collapse {
    background-color: #fff;
    border: 0;

    > .rc-collapse-item {
      > .rc-collapse-header {
        padding: 0 20px 0 0;
        position: relative;

        .arrow {
          position: absolute;
          right: 0;
          top: 50%;
          transform: translateY(-50%);
          border-top: 3px solid #666;
          border-left: 4px solid transparent;
          border-right: 4px solid transparent;
          margin-right: 0;
        }
      }
    }

    > .rc-collapse-item-active {
      > .rc-collapse-header {
        .arrow {
          border-bottom: 3px solid #666;
          border-top: 4px solid transparent;
          top: 7px;
        }
      }
    }

    > .rc-collapse-item:first-child {
      position: relative;
    }
  } */

  /* .rc-collapse-content {
    position: absolute;
    width: 150px;
    right: 20%;
    z-index: 1;
    border-radius: 3px;
    border: 1px solid #d9d9d9;

    @media (max-width: 768px) {
      position: static;
      width: auto;
    }
  } */

  @media (max-width: 768px) {
    width: 100%;

    > a {
      margin-right: 0;
    }
  }

  > a {
    color: #333;
  }

  &:not(:last-child) {
    > a {
      margin-right: 20px;
    }
  }
`;
