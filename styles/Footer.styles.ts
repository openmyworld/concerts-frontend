import styled from 'styled-components';

export const Container = styled.footer`
  text-align: center;
  padding: 30px 0;
  // background-color: #3a556a;
  background-color: #212529;
  color: #868e96;

  @media (max-width: 768px) {
    padding: 20px 0;
  }

  p {
    margin: 0;

    @media (max-width: 768px) {
      font-size: 14px;
    }
  }
`;
