import styled, { css, keyframes } from 'styled-components';

import { Button } from '@/styles/Button.styles';

type InputSubmitProps = {
  $loading: boolean;
};

const spin = keyframes`
  0% { transform: translateY(-50%) rotate(0deg); }
  100% { transform: translateY(-50%) rotate(360deg); }
`;

export const AuthContainer = styled.div`
  max-width: 500px;
  margin: auto;
  padding: 30px;
  box-shadow: 0px 10px 20px 0px rgba(50, 50, 50, 0.52);

  @media (max-width: 768px) {
    padding: 0;
    box-shadow: none;
  }
`;

export const Heading = styled.h1`
  @media (max-width: 768px) {
    font-size: 25px;
  }
`;

export const Label = styled.label`
  display: block;
  margin-bottom: 10px;
`;

export const Input = styled.input`
  display: block;
  width: 100%;
  padding: 5px;
  box-sizing: border-box;
`;

export const InputSubmit = styled(Button)<InputSubmitProps>`
  margin-top: 20px;
  width: 100%;
  font-size: 17px;
  font-weight: bold;

  @media (max-width: 768px) {
    font-size: 14px;
  }

  ${(props) =>
    props.$loading === true &&
    css`
      position: relative;
      cursor: not-allowed;
      opacity: 0.8;
      color: transparent;

      &:hover {
        background-color: #3a86ff;
      }

      &::after {
        position: absolute;
        content: '';
        width: 15px;
        height: 15px;
        top: 50%;
        left: 50%;
        border-radius: 50%;
        border: 2px solid #000;
        border-top: 2px solid #fff;
        animation: ${spin} 0.5s linear infinite;
      }
    `}
`;

export const AuthRow = styled.div`
  margin-bottom: 20px;
`;
