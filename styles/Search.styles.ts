import styled from 'styled-components';

// export const Container = styled.div`
//   @media (max-width: 768px) {
//     width: 100%;
//   }
// `;

export const Input = styled.input.attrs({
  type: 'text',
})`
  /* width: 250px; */
  width: 100%;
  padding: 5px;
  border: 1px #777 solid;
  border-radius: 5px;
  box-sizing: border-box;

  /* @media (max-width: 768px) {
    width: 100%;
  } */
`;
