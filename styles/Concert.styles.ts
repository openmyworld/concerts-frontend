import styled from 'styled-components';

export const Container = styled.div`
  position: relative;
  padding-top: 40px;
`;

export const Paragraph = styled.p`
  margin: 10px 0;

  @media (max-width: 768px) {
    font-size: 14px;
  }
`;

export const Description = styled.div`
  p {
    @media (max-width: 768px) {
      font-size: 14px;
    }
  }
`;

export const ImageContainer = styled.div`
  display: flex;
  justify-content: center;
`;

export const ImageCover = styled.figure`
  margin-bottom: 20px;
  width: 80%;

  > span {
    position: static !important;

    img {
      position: static !important;
      width: auto !important;
      height: auto !important;
    }
  }
`;

export const Back = styled.a`
  display: block;
  margin-top: 40px;
`;

export const Time = styled.time`
  @media (max-width: 768px) {
    font-size: 14px;
  }
`;
