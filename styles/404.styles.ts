import styled from 'styled-components';

export const Container = styled.div`
  text-align: center;
  margin: 100px 0 200px;
`;

export const Heading = styled.h1`
  font-size: 50px;
`;
