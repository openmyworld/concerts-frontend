import styled from 'styled-components';

export const Container = styled.div`
  margin: 50px auto;
  max-width: 1024px;
  padding: 0 20px;
`;
