import styled from 'styled-components';
import { Button } from './Button.styles';

export const Label = styled.label`
  display: block;
  margin-bottom: 5px;
`;

export const Input = styled.input`
  width: 100%;
  padding: 5px;
  box-sizing: border-box;
`;

export const InputSubmit = styled(Button)`
  display: block;
  width: 100%;
  margin: 20px 0 0 0;
`;

export const UploadFile = styled.input.attrs({ type: 'file' })`
  border: 1px #ccc solid;
  background-color: #f4f4f4;
  padding: 10px;
  width: 100%;
  box-sizing: border-box;
`;

export const Grid = styled.div`
  display: grid;
  grid-template-columns: 1fr 1fr;
  gap: 30px;
  margin-bottom: 20px;

  @media (max-width: 768px) {
    grid-template-columns: 1fr;
  }
`;

export const EditorWrap = styled.div`
  margin-bottom: 10px;
`;
