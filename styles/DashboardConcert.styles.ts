import styled from 'styled-components';
import { StyledLink } from './StyledLink.styles';

export const Container = styled.li`
  &:not(:last-child) {
    margin: 0 0 10px 0;
  }

  padding: 10px;
  border-radius: 5px;
  border: 1px #ddd solid;
  background-color: #f4f4f4;
  display: flex;
  align-items: center;
  justify-content: flex-start;
`;

export const Heading = styled.h2`
  flex: 2;
  font-size: 18px;

  @media (max-width: 768px) {
    font-size: 16px;
  }
`;

export const Anchor = styled(StyledLink)`
  margin: 10px;
  color: #333;
`;

export const DeleteLink = styled(Anchor)`
  color: #cd5c5c;
  background-image: linear-gradient(#cd5c5c, #cd5c5c);
`;

export const HandleBox = styled.div`
  @media (max-width: 768px) {
    font-size: 14px;
  }
`;
