import styled from 'styled-components';

export const StyledLink = styled.a`
	background-image: linear-gradient(#3a86ff, #3a86ff);
	background-position: center bottom;
	background-size: 0 3px;
	background-repeat: no-repeat;
	transition: background-size 0.3s;
	cursor: pointer;
	padding-bottom: 3px;

	&:hover,
	&:focus {
		background-size: 100% 3px;
	}
`;
