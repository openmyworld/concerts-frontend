import styled from 'styled-components';

export const LinkContainer = styled.a`
  display: flex;
  align-items: center;
  margin: 20px 0;
  padding: 13px;
  box-shadow: 2px 3px 5px rgba(0, 0, 0, 0.1);

  &:hover {
    opacity: 0.8;
  }
`;

export const ImageContainer = styled.figure`
  width: 30%;
  margin: 10px;
  position: relative;

  @media (max-width: 768px) {
    margin: 0 10px 0 0;
  }

  > span {
    position: relative !important;

    img {
      position: static !important;
      width: auto !important;
      height: auto !important;
      transition: all 0.5s ease-in-out;

      &:hover {
        transform: scale(1.2);
        overflow: hidden;
      }
    }
  }
`;

export const Info = styled.div`
  width: 70%;
  color: #888;

  time {
    font-size: 14px;

    @media (max-width: 768px) {
      font-size: 10px;
    }
  }
`;

export const Heading = styled.h3`
  // opacity: 0.8;
  cursor: pointer;
  color: #333;

  &:hover {
    // text-decoration: underline;
  }

  @media (max-width: 768px) {
    font-size: 16px;
  }
`;

export const Paragraph = styled.p``;
