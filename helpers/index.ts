import cookie from 'cookie';
import moment from 'moment';
import { GetServerSidePropsContext } from 'next';

type cookieToken = {
  [key: string]: string;
};

export function parseCookies(
  req: GetServerSidePropsContext['req']
): cookieToken {
  return cookie.parse(req ? (req.headers.cookie as string) : '');
}

export const formatDate = (date: string): string => {
  const result = new Date(date);

  // format() parameter error during run build

  // return Intl.DateTimeFormat('en-US', {
  //   year: 'numeric',
  //   month: 'short',
  //   day: '2-digit',
  // }).format(Date.parse(result.toString()));

  return moment(result).format('MMM DD, YYYY');
};

export const formatTime = (date: string, time: string): string => {
  return moment(new Date(`${date} ${time}`)).format('hh:mm A');
};

export const formatTimeOneParam = (input: string): string => {
  return moment(new Date(`${input}`)).format('hh:mm A');
};
