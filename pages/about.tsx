import Layout from '@/components/Layout';
import { Heading1 } from '@/styles/Heading.styles';

export default function AboutPage() {
  return (
    <Layout title='About classical music concerts'>
      <Heading1>About this site</Heading1>

      <p>This is a site to find latest and other classical music concerts.</p>

      <p>
        Marzipan jelly beans candy cake croissant tootsie roll tootsie roll
        pastry. Sesame snaps liquorice sweet marzipan chupa chups tootsie roll
        sugar plum. Topping jelly topping powder gingerbread cheesecake.
        Chocolate bar shortbread chocolate bar carrot cake oat cake. Cheesecake
        oat cake gummi bears gummi bears dragée croissant. Powder caramels oat
        cake brownie pudding sweet topping pie cake. Pie lemon drops soufflé
        halvah sweet chocolate cake fruitcake gingerbread.
      </p>

      <p>
        Apple pie candy chocolate bar chocolate sesame snaps marshmallow
        cheesecake macaroon donut. Fruitcake shortbread dessert biscuit
        marshmallow. Biscuit halvah chocolate topping marzipan lollipop jujubes.
        Jelly-o wafer chocolate tiramisu cotton candy halvah dragée cheesecake.
        Danish pastry jelly chocolate bar lemon drops tiramisu gingerbread.
        Candy tart lemon drops fruitcake candy canes cheesecake. Toffee tart
        toffee cookie marzipan lollipop tootsie roll chocolate bar cake.
      </p>

      <p>
        Macaroon topping marzipan wafer gummi bears powder chocolate wafer.
        Topping bear claw jelly-o danish powder jelly-o cupcake sugar plum.
        Cotton candy apple pie lollipop tootsie roll croissant pie candy.
        Gummies croissant carrot cake marshmallow jelly beans cotton candy
        gingerbread shortbread tootsie roll. Icing candy canes candy canes
        brownie liquorice candy tart apple pie. Cake cake dessert bonbon
        macaroon cake apple pie sweet jelly-o. Dessert caramels candy donut
        carrot cake. Dessert gingerbread halvah jelly bear claw tart chocolate
        bar. Bonbon macaroon jujubes sesame snaps jelly-o jelly-o pie chocolate
        bar.
      </p>

      <p>
        Oat cake cake tart donut toffee icing pudding. Jujubes brownie lemon
        drops brownie pastry carrot cake biscuit. Tiramisu gummi bears
        gingerbread danish halvah biscuit chocolate sugar plum apple pie.
        Brownie sugar plum gummi bears jelly beans sweet roll chocolate. Icing
        muffin topping bonbon shortbread toffee brownie. Jujubes chocolate
        sesame snaps tart pastry tart liquorice candy canes cotton candy. Ice
        cream cookie sweet roll toffee wafer danish caramels. Candy powder cake
        halvah lollipop candy canes sweet roll fruitcake cupcake. Cheesecake
        lollipop chocolate bar cheesecake brownie cotton candy pie dessert
        toffee. Fruitcake jujubes icing danish soufflé sweet apple pie.
      </p>

      <p>
        Gingerbread pudding caramels chocolate pie marzipan. Pastry jujubes
        dessert carrot cake wafer candy. Gummi bears fruitcake sugar plum chupa
        chups sugar plum cake lollipop. Dessert liquorice cotton candy soufflé
        jelly beans biscuit biscuit apple pie cake. Toffee chupa chups jujubes
        chocolate fruitcake. Pastry tiramisu dessert candy canes ice cream
        tootsie roll pudding. Pastry gummi bears marzipan pie marshmallow jelly
        tiramisu cookie.
      </p>
    </Layout>
  );
}
