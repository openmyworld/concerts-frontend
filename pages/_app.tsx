import NProgress from 'nprogress';
import 'nprogress/nprogress.css';
import Router from 'next/router';
import { AuthProvider } from '@/context/AuthContext';
import type { AppProps /*, AppContext */ } from 'next/app';

Router.events.on('routeChangeStart', () => NProgress.start());
Router.events.on('routeChangeComplete', () => NProgress.done());
Router.events.on('routeChangeError', () => NProgress.done());

function MyApp({ Component, pageProps }: AppProps) {
  return (
    <AuthProvider>
      <Component {...pageProps} />
    </AuthProvider>
  );
}

export default MyApp;
