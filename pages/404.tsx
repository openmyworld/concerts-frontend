import Link from 'next/link';
import Layout from '@/components/Layout';
import { Container, Heading } from '@/styles/404.styles';

export default function NotFoundPage() {
  return (
    <Layout title='Page Not Found'>
      <Container>
        <Heading>404 NOT FOUND</Heading>
        <Link href='/'>Go to homepage</Link>
      </Container>
    </Layout>
  );
}
