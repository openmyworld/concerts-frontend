import Layout from '@/components/Layout';
import ConcertItem from '@/components/ConcertItem';
import { API_URL, PER_PAGE } from '@/config/index';
import Pagination from '@/components/Pagination';
import { GetServerSideProps } from 'next';
import type { ConcertPer } from '../index';
import { Ul } from '@/styles/ConcertList.styles';
import { Heading1 } from '@/styles/Heading.styles';

type Props = {
  concertsProps: [];
  page: number;
  total: number;
};

export default function ConcertsPage({ concertsProps, page, total }: Props) {
  return (
    <Layout>
      <Heading1>Concerts</Heading1>
      {concertsProps.length === 0 && <h3>There are no concerts</h3>}
      <Ul>
        {concertsProps.map((concert: ConcertPer) => (
          <ConcertItem key={concert.id} concert={concert} />
        ))}
      </Ul>

      <Pagination page={page} total={total} />
    </Layout>
  );
}
export const getServerSideProps: GetServerSideProps = async ({
  query: { page = 1 },
}) => {
  // Convert page's string to number
  const pageNumber = (page as number) * 1;

  // Calculate start page
  const start = pageNumber === 1 ? 0 : (pageNumber - 1) * PER_PAGE;

  // Start page works like below
  // per_page: 2
  // if page = 1 then ( (page = 0) * 2 ) = 0.  start = 0
  // if page = 2 then ( (page = 1) * 2 ) = 2.  start = 2
  // if page = 3 then ( (page = 2) * 2 ) = 4.  start = 4
  // if page = 5 then ( (page = 4) * 2 ) = 8.  start = 8
  // if page = 4 then ( (page = 3) * 2 ) = 6.  start = 6
  // if page = 6 then ( (page = 5) * 2 ) = 10. start = 10
  // if page = 7 then ( (page = 6) * 2 ) = 12. start = 12

  // Fetch total concerts
  const totalResponse = await fetch(`${API_URL}/concerts/count`);
  const total = await totalResponse.json();

  // Fetch concerts results
  const concertResponse = await fetch(
    `${API_URL}/concerts?_sort=id:DESC&_limit=${PER_PAGE}&_start=${start}`
  );
  const concerts = await concertResponse.json();

  return {
    props: {
      concertsProps: concerts,
      page: pageNumber,
      total,
    },
  };
};
