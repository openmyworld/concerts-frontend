import moment from 'moment';
import { parseCookies } from '@/helpers/index';
import React, { useState } from 'react';
import { useRouter } from 'next/router';
import { API_URL } from '@/config/index';
import { ToastContainer, toast } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';
import Layout from '@/components/Layout';

import {
  Grid,
  Label,
  UploadFile,
  Input,
  InputSubmit,
  EditorWrap,
} from '@/styles/Form.styles';

import { Heading1 } from '@/styles/Heading.styles';
import { GetServerSideProps } from 'next';
import dynamic from 'next/dynamic';
import 'react-markdown-editor-lite/lib/index.css';
import { marked } from 'marked';
import Editor, { Plugins } from 'react-markdown-editor-lite';

const MdEditor = dynamic(() => import('react-markdown-editor-lite'), {
  ssr: false,
});

// Remove plugin
Editor.unuse(Plugins.Header);
Editor.unuse(Plugins.FontUnderline);
Editor.unuse(Plugins.Image);
Editor.unuse(Plugins.Table);

marked.setOptions({
  breaks: true,
});

type Props = {
  token: string;
};

export default function AddConcert({ token }: Props) {
  const [values, setValues] = useState({
    name: '',
    performers: '',
    venue: '',
    address: '',
    date: '',
    time: '',
    description: '',
  });

  const [image, setImage] = useState<{ name: string; file: File } | null>(null);

  const router = useRouter();

  const handleSubmit = async (e: React.FormEvent<HTMLFormElement>) => {
    e.preventDefault();

    const hasEmptyFields = Object.values(values).some(
      (element) => element === ''
    );

    if (hasEmptyFields) {
      toast.error('Please fill in all fields');
      return;
    }

    const formData = new FormData();

    // Append other fields
    formData.append(
      'data',
      JSON.stringify({
        ...values,
        time: moment(`${values.date} ${values.time}`).format('HH:mm:ss.SSS'),
      })
    );

    // Append image field, use If statament for TypeScript
    if (image !== null) {
      formData.append(`files.${image.name}`, image.file);
    }

    const response = await fetch(`${API_URL}/concerts/`, {
      method: 'POST',
      headers: {
        Authorization: `Bearer ${token}`,
      },
      body: formData,
    });

    if (!response.ok) {
      if (response.status === 403 || response.status === 401) {
        toast.error('No token included');
        return;
      }
      toast.error('Something went wrong');
    } else {
      const result = await response.json();
      router.push(`/concerts/${result.slug}`);
    }
  };

  const handleInputChange = (
    e: React.ChangeEvent<HTMLInputElement | HTMLTextAreaElement>
  ) => {
    const { name, value } = e.target;
    setValues({ ...values, [name]: value });
  };

  const handleImageChange = (e: React.ChangeEvent<HTMLInputElement>) => {
    if (e.target.files !== null) {
      setImage({
        name: e.target.name,
        file: e.target.files[0],
      });
    }
  };

  function handleEditorChange({ text }: { text: string }) {
    setValues({ ...values, description: text });
  }

  return (
    <Layout title='Add new concert'>
      <Heading1>Add concert</Heading1>
      <ToastContainer />
      <form onSubmit={handleSubmit}>
        <Grid>
          <div>
            <Label htmlFor='name'>Concert Name</Label>
            <Input
              type='text'
              id='name'
              name='name'
              value={values.name}
              required
              onChange={handleInputChange}
            />
          </div>
          <div>
            <Label htmlFor='performers'>Performers</Label>
            <Input
              type='text'
              id='performers'
              name='performers'
              value={values.performers}
              required
              onChange={handleInputChange}
            />
          </div>
          <div>
            <Label htmlFor='venue'>Venue</Label>
            <Input
              type='text'
              id='venue'
              name='venue'
              value={values.venue}
              required
              onChange={handleInputChange}
            />
          </div>
          <div>
            <Label htmlFor='address'>Address</Label>
            <Input
              type='text'
              id='address'
              name='address'
              value={values.address}
              required
              onChange={handleInputChange}
            />
          </div>
          <div>
            <Label htmlFor='date'>Date</Label>
            <Input
              type='date'
              id='date'
              name='date'
              value={values.date}
              required
              onChange={handleInputChange}
            />
          </div>
          <div>
            <Label htmlFor='time'>Time</Label>
            <Input
              type='time'
              id='time'
              name='time'
              value={values.time}
              required
              onChange={handleInputChange}
            />
          </div>
        </Grid>

        <EditorWrap>
          <Label htmlFor='description'>Description</Label>
          <MdEditor
            id='description'
            style={{ height: '300px' }}
            renderHTML={(text) => marked(text)}
            onChange={handleEditorChange}
          />
        </EditorWrap>

        <div>
          <Label>Image</Label>
          <UploadFile
            type='file'
            name='image'
            accept='image/*'
            onChange={handleImageChange}
          />
        </div>

        <InputSubmit as='input' type='submit' value='Add' />
      </form>
    </Layout>
  );
}

export const getServerSideProps: GetServerSideProps = async ({ req }) => {
  const { headers } = req;

  if (headers.cookie) {
    const { token } = parseCookies(req);

    const response = await fetch(`${API_URL}/concerts/me`, {
      method: 'GET',
      headers: {
        Authorization: `Bearer ${token}`,
      },
    });

    if (response.ok) {
      return {
        props: {
          token,
        },
      };
    } else {
      return {
        redirect: {
          destination: '/account/login',
          permanent: false,
        },
      };
    }
  } else {
    return {
      redirect: {
        destination: '/account/login',
        permanent: false,
      },
    };
  }
};
