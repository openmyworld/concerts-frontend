import React from 'react';
import moment from 'moment';
import { parseCookies } from '@/helpers/index';
import { useState } from 'react';
import { useRouter } from 'next/router';
import { API_URL } from '@/config/index';
import { ToastContainer, toast } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';
import Layout from '@/components/Layout';
import Modal from '@/components/Modal';
import ImageUpload from '@/components/ImageUpload';

import {
  Grid,
  Label,
  Input,
  InputSubmit,
  EditorWrap,
} from '@/styles/Form.styles';

import { GetServerSideProps } from 'next';
import { ParsedUrlQuery } from 'querystring';
import { ButtonSecondary } from '@/styles/Button.styles';
import Image from 'next/image';
import dynamic from 'next/dynamic';
import 'react-markdown-editor-lite/lib/index.css';
import { marked } from 'marked';
import { Heading1 } from '@/styles/Heading.styles';

const MdEditor = dynamic(() => import('react-markdown-editor-lite'), {
  ssr: false,
});

import Editor, { Plugins } from 'react-markdown-editor-lite';

Editor.unuse(Plugins.Header);
Editor.unuse(Plugins.FontUnderline);
Editor.unuse(Plugins.Image);
Editor.unuse(Plugins.Table);

marked.setOptions({
  breaks: true,
});

type Props = {
  concertProps: {
    name: string;
    performers: string;
    venue: string;
    address: string;
    date: string;
    time: string;
    description: string;
    id: number;
    image: {
      formats: {
        thumbnail: {
          url: string;
        };
      };
    };
  };
  token: string;
};

interface IParams extends ParsedUrlQuery {
  id: string;
}

export default function EditConcertPage({ concertProps, token }: Props) {
  const [values, setValues] = useState({
    name: concertProps.name,
    performers: concertProps.performers,
    venue: concertProps.venue,
    address: concertProps.address,
    date: concertProps.date,
    time: concertProps.time,
    description: concertProps.description,
  });

  const [imagePreview, setImagePreview] = useState<string | null>(
    concertProps.image ? concertProps.image.formats.thumbnail.url : null
  );

  const [showModal, setShowModal] = useState(false);

  const router = useRouter();

  const handleSubmit = async (e: React.FormEvent<HTMLFormElement>) => {
    e.preventDefault();

    const hasEmptyFields = Object.values(values).some(
      (element) => element === ''
    );

    if (hasEmptyFields) {
      toast.error('Please fill in all fields');
    }

    const response = await fetch(`${API_URL}/concerts/${concertProps.id}`, {
      method: 'PUT',
      headers: {
        'Content-Type': 'application/json',
        Authorization: `Bearer ${token}`,
      },
      body: JSON.stringify({
        ...values,
        time: moment(`${values.date} ${values.time}`).format('HH:mm:ss.SSS'),
      }),
    });

    if (!response.ok) {
      if (response.status === 403 || response.status === 401) {
        toast.error('No token included');
        return;
      }
      toast.error(`Something went wrong, ${response.status}`);
    } else {
      const concertResponse = await response.json();
      router.push(`/concerts/${concertResponse.slug}`);
    }
  };

  const handleInputChange = (
    e: React.ChangeEvent<HTMLInputElement | HTMLTextAreaElement>
  ): void => {
    const { name, value } = e.target;
    setValues({ ...values, [name]: value });
  };

  const imageUploaded = async () => {
    const response = await fetch(`${API_URL}/concerts/${concertProps.id}`);
    const data = await response.json();
    setImagePreview(data.image.formats.thumbnail.url);
    setShowModal(false);
  };

  function handleEditorChange({ text }: { text: string }) {
    setValues({ ...values, description: text });
  }

  return (
    <Layout title={`Edit concert "${concertProps.name}"`}>
      <Heading1>Edit concert</Heading1>
      <ToastContainer />
      <form onSubmit={handleSubmit}>
        <Grid>
          <div>
            <Label htmlFor='name'>Name</Label>
            <Input
              type='text'
              id='name'
              name='name'
              value={values.name}
              onChange={handleInputChange}
            />
          </div>
          <div>
            <Label htmlFor='performers'>Performers</Label>
            <Input
              type='text'
              id='performers'
              name='performers'
              value={values.performers}
              onChange={handleInputChange}
            />
          </div>
          <div>
            <Label htmlFor='venue'>Venue</Label>
            <Input
              type='text'
              id='venue'
              name='venue'
              value={values.venue}
              onChange={handleInputChange}
            />
          </div>
          <div>
            <Label htmlFor='address'>Address</Label>
            <Input
              type='text'
              id='address'
              name='address'
              value={values.address}
              onChange={handleInputChange}
            />
          </div>
          <div>
            <Label htmlFor='date'>Date</Label>
            <Input
              type='date'
              id='date'
              name='date'
              value={moment(values.date).format('yyyy-MM-DD')}
              onChange={handleInputChange}
            />
          </div>
          <div>
            <Label htmlFor='time'>Time</Label>
            <Input
              type='time'
              id='time'
              name='time'
              value={values.time}
              onChange={handleInputChange}
            />
          </div>
        </Grid>

        <EditorWrap>
          <Label htmlFor='description'>Description</Label>
          <MdEditor
            style={{ height: '400px' }}
            value={values.description ? values.description : ''}
            renderHTML={(text) => marked(text)}
            onChange={handleEditorChange}
          />
        </EditorWrap>

        <div>
          <Label>Image</Label>
          {imagePreview && (
            <Image
              width={200}
              height={200}
              src={imagePreview}
              alt='Image Preview'
            />
          )}
        </div>

        <div>
          <ButtonSecondary onClick={() => setShowModal(true)}>
            Set Image
          </ButtonSecondary>
        </div>

        <InputSubmit as='input' type='submit' value='Update' />
      </form>

      <Modal show={showModal} onClose={() => setShowModal(false)}>
        <ImageUpload
          concertId={concertProps.id}
          imageUploaded={imageUploaded}
          token={token}
        />
      </Modal>
    </Layout>
  );
}

export const getServerSideProps: GetServerSideProps = async (context) => {
  const { id } = context.params as IParams;

  if (context.req.headers.cookie) {
    const { token } = parseCookies(context.req);
    const response = await fetch(`${API_URL}/concerts/${id}`);
    const concert = await response.json();

    if (response.ok) {
      return {
        props: {
          concertProps: concert,
          token,
        },
      };
    } else {
      return {
        props: {},
      };
    }
  } else {
    return {
      redirect: {
        destination: '/account/login',
        permanent: false,
      },
    };
  }
};
