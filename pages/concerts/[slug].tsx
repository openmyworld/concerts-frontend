import Link from 'next/link';
import Image from 'next/image';
import Layout from '@/components/Layout';
import { API_URL } from '@/config/index';

import {
  Container,
  Paragraph,
  Back,
  ImageContainer,
  ImageCover,
  Description,
  Time,
} from '@/styles/Concert.styles';

import { Heading1, Heading2 } from '@/styles/Heading.styles';

import { formatDate, formatTime, formatTimeOneParam } from '@/helpers/index';
import { GetStaticProps } from 'next';
import { ParsedUrlQuery } from 'querystring';
import type { ConcertPer } from '../index';
import ConcertMap from '../../components/ConcertMap';

import { marked } from 'marked';

marked.setOptions({
  breaks: true,
});

// Default value during build
const defaultConcertValue = {
  id: 1,
  name: '',
  slug: '',
  venue: '',
  address: '',
  date: '',
  performers: '',
  description: '',
  time: '',
  published_at: '',
  // updated_at: '',
  image: {
    formats: {
      medium: {
        url: '',
      },
    },
  },
};

type Props = {
  concertProps: {
    id: number;
    name: string;
    date: string;
    time: string;
    performers: string;
    description: string;
    image: {
      formats: {
        medium: {
          url: string;
        };
      };
    };
    address: string;
    venue: string;
    published_at: string;
  };
};

interface IParams extends ParsedUrlQuery {
  slug: string;
}

export default function ConcertPage({
  concertProps = defaultConcertValue,
}: Props) {
  const datePublish = formatDate(concertProps.published_at);
  const timePublish = formatTimeOneParam(concertProps.published_at);
  const date = formatDate(concertProps.date);
  const time = formatTime(concertProps.date, concertProps.time);

  return (
    <Layout title={concertProps.name}>
      <Container>
        <Time dateTime={datePublish}>
          {datePublish} at {timePublish}
        </Time>

        <Heading1>{concertProps.name}</Heading1>

        <ImageContainer>
          <ImageCover>
            <Image
              src={
                concertProps.image
                  ? concertProps.image.formats.medium.url
                  : '/images/no-image.jpg'
              }
              layout='fill'
              alt={concertProps.name}
            />
          </ImageCover>
        </ImageContainer>

        <Heading2>Performer</Heading2>
        <Paragraph>{concertProps.performers}</Paragraph>

        <Heading2>Description</Heading2>
        <Description
          dangerouslySetInnerHTML={{ __html: marked(concertProps.description) }}
        />

        <Heading2>Date</Heading2>
        <Paragraph>
          <time dateTime={date}>{date}</time>
        </Paragraph>

        <Heading2>Time</Heading2>
        <Paragraph>
          <time dateTime={date}>{time}</time>
        </Paragraph>

        <Heading2>Venue</Heading2>
        <Paragraph>{concertProps.venue}</Paragraph>

        <Heading2>Address</Heading2>
        <Paragraph>{concertProps.address}</Paragraph>

        <ConcertMap address={concertProps.address} id={concertProps.id} />

        <Link href='/concerts' passHref>
          <Back>{'<'} Go back</Back>
        </Link>
      </Container>
    </Layout>
  );
}

export async function getStaticPaths() {
  const response = await fetch(`${API_URL}/concerts`);
  const concerts = await response.json();

  const paths = concerts.map((concertPer: ConcertPer) => ({
    params: { slug: concertPer.slug },
  }));

  return {
    paths: paths,
    fallback: true,
  };
}

export const getStaticProps: GetStaticProps = async (context) => {
  const { slug } = context.params as IParams;
  const response = await fetch(`${API_URL}/concerts?slug=${slug}`);
  const concerts = await response.json();

  return {
    props: {
      concertProps: concerts[0],
    },
    revalidate: 1,
  };
};
