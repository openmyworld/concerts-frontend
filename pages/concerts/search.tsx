import Layout from '@/components/Layout';
import ConcertItem from '@/components/ConcertItem';
import { API_URL } from '@/config/index';
import qs from 'qs';
import { useRouter } from 'next/router';
import { GetServerSideProps } from 'next';
import type { ConcertPer } from '../index';
import { Ul } from '@/styles/ConcertList.styles';
import { Heading1 } from '@/styles/Heading.styles';

type Props = {
  concertsProps: [];
};

export default function SearchPage({ concertsProps }: Props) {
  const router = useRouter();

  return (
    <Layout title='Search results'>
      <Heading1>Search Results for &apos;{router.query.term}&apos;</Heading1>
      {concertsProps.length === 0 && <h3>There are no concerts</h3>}
      <Ul>
        {concertsProps.map((concertPer: ConcertPer) => (
          <ConcertItem key={concertPer.id} concert={concertPer} />
        ))}
      </Ul>
    </Layout>
  );
}

export const getServerSideProps: GetServerSideProps = async ({
  query: { term },
}) => {
  const query = qs.stringify({
    _where: {
      _or: [
        { name_contains: term },
        { performers_contains: term },
        { description_contains: term },
        { venue_contains: term },
      ],
    },
  });

  const response = await fetch(`${API_URL}/concerts?${query}`);
  const concerts = await response.json();

  return {
    props: {
      concertsProps: concerts,
    },
  };
};
