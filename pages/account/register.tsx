import { ToastContainer, toast } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';
import React, { useState, useEffect, useContext } from 'react';
import Layout from '@/components/Layout';

import {
  AuthContainer,
  Heading,
  Label,
  AuthRow,
  Input,
  InputSubmit,
} from '@/styles/AuthForm.styles';

import Link from 'next/link';
import AuthContext, { contextValue } from '@/context/AuthContext';
import { GetServerSideProps } from 'next';
import { parseCookies } from '@/helpers/index';
import { API_URL } from '@/config/index';

export default function RegisterPage() {
  const [username, setUsername] = useState('');
  const [email, setEmail] = useState('');
  const [password, setPassword] = useState('');
  const [passwordConfirm, setPasswordConfirm] = useState('');
  const { register, error, loadingRegister, setLoadingRegister } = useContext(
    AuthContext
  ) as contextValue;

  const handleSubmit = (e: React.FormEvent<HTMLFormElement>) => {
    e.preventDefault();

    setLoadingRegister(true);

    if (password !== passwordConfirm) {
      toast.error('Password do not match!');
      return;
    }

    register({ username, email, password });
  };

  useEffect(() => {
    error && toast.error(error);
  });

  return (
    <Layout title='User Registration'>
      <AuthContainer>
        <Heading>Register</Heading>
        <ToastContainer />
        <form onSubmit={handleSubmit}>
          <AuthRow>
            <Label htmlFor='username'>Username</Label>
            <Input
              type='text'
              id='username'
              value={username}
              required
              onChange={(e) => setUsername(e.target.value)}
            />
          </AuthRow>

          <AuthRow>
            <Label htmlFor='email'>Email Address</Label>
            <Input
              type='email'
              id='email'
              value={email}
              required
              onChange={(e) => setEmail(e.target.value)}
            />
          </AuthRow>

          <AuthRow>
            <Label htmlFor='password'>Password</Label>
            <Input
              type='password'
              id='password'
              value={password}
              required
              onChange={(e) => setPassword(e.target.value)}
            />
          </AuthRow>

          <AuthRow>
            <Label htmlFor='passwordConfirm'>Confirm Password</Label>
            <Input
              type='password'
              id='passwordConfirm'
              value={passwordConfirm}
              required
              onChange={(e) => setPasswordConfirm(e.target.value)}
            />
          </AuthRow>

          <InputSubmit
            as='button'
            type='submit'
            disabled={loadingRegister}
            $loading={loadingRegister}
          >
            Register
          </InputSubmit>

          <p>
            Already have account? <Link href='/account/login'>Login</Link>
          </p>
        </form>
      </AuthContainer>
    </Layout>
  );
}

export const getServerSideProps: GetServerSideProps = async ({ req }) => {
  const { headers } = req;

  if (headers.cookie) {
    const { token } = parseCookies(req);

    const response = await fetch(`${API_URL}/concerts/me`, {
      method: 'GET',
      headers: {
        Authorization: `Bearer ${token}`,
      },
    });

    if (response.ok) {
      return {
        redirect: {
          destination: '/account/dashboard',
          permanent: false,
        },
      };
    } else {
      return {
        props: {},
      };
    }
  } else {
    return {
      props: {},
    };
  }
};
