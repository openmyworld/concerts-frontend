import { useRouter } from 'next/router';
import { ToastContainer, toast } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';
import { parseCookies } from '@/helpers/index';
import { API_URL } from '@/config/index';
import Layout from '@/components/Layout';
import DashboardConcert from '@/components/DashboardConcert';
import { GetServerSideProps } from 'next';
import type { ConcertPer } from '../index';
import { Ul } from '@/styles/ConcertList.styles';
import { Heading1 } from '@/styles/Heading.styles';

type Props = {
  concertsProps: [];
  token: string;
};

export default function DashboardPage({ concertsProps, token }: Props) {
  const router = useRouter();

  const deleteConcert = async (id: number) => {
    if (confirm('Are you sure?')) {
      const response = await fetch(`${API_URL}/concerts/${id}`, {
        method: 'DELETE',
        headers: {
          Authorization: `Bearer ${token}`,
        },
      });

      const result = await response.json();

      if (!response.ok) {
        toast.error(result.message);
      } else {
        router.reload();
      }
    }
  };

  return (
    <Layout title='Dashboard'>
      <Heading1>My concerts</Heading1>
      <ToastContainer />
      <Ul>
        {concertsProps.map((concertPer: ConcertPer) => (
          <DashboardConcert
            key={concertPer.id}
            concert={concertPer}
            handleDelete={deleteConcert}
          />
        ))}
      </Ul>
    </Layout>
  );
}

export const getServerSideProps: GetServerSideProps = async ({ req }) => {
  const { headers } = req;

  if (headers.cookie) {
    const { token } = parseCookies(req);

    const response = await fetch(`${API_URL}/concerts/me`, {
      method: 'GET',
      headers: {
        Authorization: `Bearer ${token}`,
      },
    });

    const concerts = await response.json();

    if (response.ok) {
      return {
        props: {
          concertsProps: concerts,
          token,
        },
      };
    } else {
      return {
        redirect: {
          destination: '/account/login',
          permanent: false,
        },
      };
    }
  } else {
    return {
      redirect: {
        destination: '/account/login',
        permanent: false,
      },
    };
  }
};
