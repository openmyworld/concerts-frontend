import Layout from '@/components/Layout';
import ConcertItem from '@/components/ConcertItem';
import { API_URL } from '@/config/index';
import Link from 'next/link';
import { GetStaticProps } from 'next';
import { ButtonSecondary } from '@/styles/Button.styles';
import { Ul, ViewConcertContainer } from '@/styles/ConcertList.styles';
import { Heading2 } from '@/styles/Heading.styles';

type Props = {
  concertsProps: [];
};

export type ConcertPer = {
  id: number;
  name: string;
  published_at: string;
  address: string;
  image: {
    formats: {
      thumbnail: {
        url: string;
      };
    };
  };
  slug: string;
};

export default function Home({ concertsProps }: Props) {
  return (
    <Layout>
      <Heading2>Upcoming concerts</Heading2>
      {concertsProps.length === 0 && <h3>There are no concerts</h3>}

      <Ul>
        {concertsProps.map((concert: ConcertPer) => (
          <ConcertItem key={concert.id} concert={concert} />
        ))}
      </Ul>

      {concertsProps.length > 0 && (
        <ViewConcertContainer>
          <Link href='/concerts' passHref>
            <ButtonSecondary>View all concerts</ButtonSecondary>
          </Link>
        </ViewConcertContainer>
      )}
    </Layout>
  );
}

export const getStaticProps: GetStaticProps = async () => {
  const response = await fetch(`${API_URL}/concerts?_sort=id:DESC&_limit=3`);
  const concerts = await response.json();

  return {
    props: {
      concertsProps: concerts,
    },
    revalidate: 1,
  };
};
